--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.11
-- Dumped by pg_dump version 9.5.0

-- Started on 2016-03-01 14:38:15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 16386)
-- Name: roper; Type: SCHEMA; Schema: -; Owner: pgadmin
--

CREATE SCHEMA roper;


ALTER SCHEMA roper OWNER TO pgadmin;

SET search_path = roper, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 174 (class 1259 OID 16412)
-- Name: collection; Type: TABLE; Schema: roper; Owner: pgadmin
--

CREATE TABLE collection (
    id integer NOT NULL,
    name character varying(100),
    description text,
    data_provider_id integer NOT NULL,
    folder character varying(260)
);


ALTER TABLE collection OWNER TO pgadmin;

--
-- TOC entry 173 (class 1259 OID 16410)
-- Name: collection_data_provider_id_seq; Type: SEQUENCE; Schema: roper; Owner: pgadmin
--

CREATE SEQUENCE collection_data_provider_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE collection_data_provider_id_seq OWNER TO pgadmin;

--
-- TOC entry 2037 (class 0 OID 0)
-- Dependencies: 173
-- Name: collection_data_provider_id_seq; Type: SEQUENCE OWNED BY; Schema: roper; Owner: pgadmin
--

ALTER SEQUENCE collection_data_provider_id_seq OWNED BY collection.data_provider_id;


--
-- TOC entry 172 (class 1259 OID 16408)
-- Name: collection_id_seq; Type: SEQUENCE; Schema: roper; Owner: pgadmin
--

CREATE SEQUENCE collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE collection_id_seq OWNER TO pgadmin;

--
-- TOC entry 2038 (class 0 OID 0)
-- Dependencies: 172
-- Name: collection_id_seq; Type: SEQUENCE OWNED BY; Schema: roper; Owner: pgadmin
--

ALTER SEQUENCE collection_id_seq OWNED BY collection.id;


--
-- TOC entry 176 (class 1259 OID 16424)
-- Name: data_provider; Type: TABLE; Schema: roper; Owner: pgadmin
--

CREATE TABLE data_provider (
    id integer NOT NULL,
    provider_name character varying(100)
);


ALTER TABLE data_provider OWNER TO pgadmin;

--
-- TOC entry 175 (class 1259 OID 16422)
-- Name: data_provider_id_seq; Type: SEQUENCE; Schema: roper; Owner: pgadmin
--

CREATE SEQUENCE data_provider_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data_provider_id_seq OWNER TO pgadmin;

--
-- TOC entry 2039 (class 0 OID 0)
-- Dependencies: 175
-- Name: data_provider_id_seq; Type: SEQUENCE OWNED BY; Schema: roper; Owner: pgadmin
--

ALTER SEQUENCE data_provider_id_seq OWNED BY data_provider.id;


--
-- TOC entry 180 (class 1259 OID 16462)
-- Name: file; Type: TABLE; Schema: roper; Owner: pgadmin
--

CREATE TABLE file (
    id integer NOT NULL,
    archive_number character varying(100),
    study_id character varying(50),
    filename character varying(260)
);


ALTER TABLE file OWNER TO pgadmin;

--
-- TOC entry 181 (class 1259 OID 16480)
-- Name: file_extension; Type: TABLE; Schema: roper; Owner: pgadmin
--

CREATE TABLE file_extension (
    extension character varying(260) NOT NULL,
    name character varying(100),
    description text
);


ALTER TABLE file_extension OWNER TO pgadmin;

--
-- TOC entry 179 (class 1259 OID 16460)
-- Name: file_id_seq; Type: SEQUENCE; Schema: roper; Owner: pgadmin
--

CREATE SEQUENCE file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE file_id_seq OWNER TO pgadmin;

--
-- TOC entry 2040 (class 0 OID 0)
-- Dependencies: 179
-- Name: file_id_seq; Type: SEQUENCE OWNED BY; Schema: roper; Owner: pgadmin
--

ALTER SEQUENCE file_id_seq OWNED BY file.id;


--
-- TOC entry 183 (class 1259 OID 16490)
-- Name: file_type; Type: TABLE; Schema: roper; Owner: pgadmin
--

CREATE TABLE file_type (
    id smallint NOT NULL,
    name character varying(100),
    description text
);


ALTER TABLE file_type OWNER TO pgadmin;

--
-- TOC entry 182 (class 1259 OID 16488)
-- Name: file_type_id_seq; Type: SEQUENCE; Schema: roper; Owner: pgadmin
--

CREATE SEQUENCE file_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE file_type_id_seq OWNER TO pgadmin;

--
-- TOC entry 2041 (class 0 OID 0)
-- Dependencies: 182
-- Name: file_type_id_seq; Type: SEQUENCE OWNED BY; Schema: roper; Owner: pgadmin
--

ALTER SEQUENCE file_type_id_seq OWNED BY file_type.id;


--
-- TOC entry 178 (class 1259 OID 16449)
-- Name: study; Type: TABLE; Schema: roper; Owner: pgadmin
--

CREATE TABLE study (
    archive_number character(100) NOT NULL,
    study_id character varying(50) NOT NULL,
    collection_id integer NOT NULL
);


ALTER TABLE study OWNER TO pgadmin;

--
-- TOC entry 177 (class 1259 OID 16447)
-- Name: study_collection_id_seq; Type: SEQUENCE; Schema: roper; Owner: pgadmin
--

CREATE SEQUENCE study_collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE study_collection_id_seq OWNER TO pgadmin;

--
-- TOC entry 2042 (class 0 OID 0)
-- Dependencies: 177
-- Name: study_collection_id_seq; Type: SEQUENCE OWNED BY; Schema: roper; Owner: pgadmin
--

ALTER SEQUENCE study_collection_id_seq OWNED BY study.collection_id;


--
-- TOC entry 1894 (class 2604 OID 16415)
-- Name: id; Type: DEFAULT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY collection ALTER COLUMN id SET DEFAULT nextval('collection_id_seq'::regclass);


--
-- TOC entry 1895 (class 2604 OID 16416)
-- Name: data_provider_id; Type: DEFAULT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY collection ALTER COLUMN data_provider_id SET DEFAULT nextval('collection_data_provider_id_seq'::regclass);


--
-- TOC entry 1896 (class 2604 OID 16427)
-- Name: id; Type: DEFAULT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY data_provider ALTER COLUMN id SET DEFAULT nextval('data_provider_id_seq'::regclass);


--
-- TOC entry 1898 (class 2604 OID 16465)
-- Name: id; Type: DEFAULT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY file ALTER COLUMN id SET DEFAULT nextval('file_id_seq'::regclass);


--
-- TOC entry 1899 (class 2604 OID 16493)
-- Name: id; Type: DEFAULT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY file_type ALTER COLUMN id SET DEFAULT nextval('file_type_id_seq'::regclass);


--
-- TOC entry 1897 (class 2604 OID 16452)
-- Name: collection_id; Type: DEFAULT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY study ALTER COLUMN collection_id SET DEFAULT nextval('study_collection_id_seq'::regclass);


--
-- TOC entry 2023 (class 0 OID 16412)
-- Dependencies: 174
-- Data for Name: collection; Type: TABLE DATA; Schema: roper; Owner: pgadmin
--

COPY collection (id, name, description, data_provider_id, folder) FROM stdin;
\.


--
-- TOC entry 2043 (class 0 OID 0)
-- Dependencies: 173
-- Name: collection_data_provider_id_seq; Type: SEQUENCE SET; Schema: roper; Owner: pgadmin
--

SELECT pg_catalog.setval('collection_data_provider_id_seq', 1, false);


--
-- TOC entry 2044 (class 0 OID 0)
-- Dependencies: 172
-- Name: collection_id_seq; Type: SEQUENCE SET; Schema: roper; Owner: pgadmin
--

SELECT pg_catalog.setval('collection_id_seq', 1, false);


--
-- TOC entry 2025 (class 0 OID 16424)
-- Dependencies: 176
-- Data for Name: data_provider; Type: TABLE DATA; Schema: roper; Owner: pgadmin
--

COPY data_provider (id, provider_name) FROM stdin;
\.


--
-- TOC entry 2045 (class 0 OID 0)
-- Dependencies: 175
-- Name: data_provider_id_seq; Type: SEQUENCE SET; Schema: roper; Owner: pgadmin
--

SELECT pg_catalog.setval('data_provider_id_seq', 1, false);


--
-- TOC entry 2029 (class 0 OID 16462)
-- Dependencies: 180
-- Data for Name: file; Type: TABLE DATA; Schema: roper; Owner: pgadmin
--

COPY file (id, archive_number, study_id, filename) FROM stdin;
\.


--
-- TOC entry 2030 (class 0 OID 16480)
-- Dependencies: 181
-- Data for Name: file_extension; Type: TABLE DATA; Schema: roper; Owner: pgadmin
--

COPY file_extension (extension, name, description) FROM stdin;
\.


--
-- TOC entry 2046 (class 0 OID 0)
-- Dependencies: 179
-- Name: file_id_seq; Type: SEQUENCE SET; Schema: roper; Owner: pgadmin
--

SELECT pg_catalog.setval('file_id_seq', 1, false);


--
-- TOC entry 2032 (class 0 OID 16490)
-- Dependencies: 183
-- Data for Name: file_type; Type: TABLE DATA; Schema: roper; Owner: pgadmin
--

COPY file_type (id, name, description) FROM stdin;
\.


--
-- TOC entry 2047 (class 0 OID 0)
-- Dependencies: 182
-- Name: file_type_id_seq; Type: SEQUENCE SET; Schema: roper; Owner: pgadmin
--

SELECT pg_catalog.setval('file_type_id_seq', 1, false);


--
-- TOC entry 2027 (class 0 OID 16449)
-- Dependencies: 178
-- Data for Name: study; Type: TABLE DATA; Schema: roper; Owner: pgadmin
--

COPY study (archive_number, study_id, collection_id) FROM stdin;
\.


--
-- TOC entry 2048 (class 0 OID 0)
-- Dependencies: 177
-- Name: study_collection_id_seq; Type: SEQUENCE SET; Schema: roper; Owner: pgadmin
--

SELECT pg_catalog.setval('study_collection_id_seq', 1, false);


--
-- TOC entry 1901 (class 2606 OID 16421)
-- Name: collection_pk; Type: CONSTRAINT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY collection
    ADD CONSTRAINT collection_pk PRIMARY KEY (id);


--
-- TOC entry 1903 (class 2606 OID 16429)
-- Name: data_provider_pk; Type: CONSTRAINT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY data_provider
    ADD CONSTRAINT data_provider_pk PRIMARY KEY (id);


--
-- TOC entry 1909 (class 2606 OID 16487)
-- Name: file_extension_pk; Type: CONSTRAINT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY file_extension
    ADD CONSTRAINT file_extension_pk PRIMARY KEY (extension);


--
-- TOC entry 1907 (class 2606 OID 16467)
-- Name: file_pk; Type: CONSTRAINT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY file
    ADD CONSTRAINT file_pk PRIMARY KEY (id);


--
-- TOC entry 1911 (class 2606 OID 16498)
-- Name: file_type_pk; Type: CONSTRAINT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY file_type
    ADD CONSTRAINT file_type_pk PRIMARY KEY (id);


--
-- TOC entry 1905 (class 2606 OID 16454)
-- Name: study_pk; Type: CONSTRAINT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY study
    ADD CONSTRAINT study_pk PRIMARY KEY (archive_number, study_id);


--
-- TOC entry 1913 (class 2606 OID 16468)
-- Name: file_study_fk; Type: FK CONSTRAINT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY file
    ADD CONSTRAINT file_study_fk FOREIGN KEY (archive_number, study_id) REFERENCES study(archive_number, study_id);


--
-- TOC entry 1912 (class 2606 OID 16455)
-- Name: study_collection_id_fk; Type: FK CONSTRAINT; Schema: roper; Owner: pgadmin
--

ALTER TABLE ONLY study
    ADD CONSTRAINT study_collection_id_fk FOREIGN KEY (collection_id) REFERENCES collection(id);


-- Completed on 2016-03-01 14:38:15

--
-- PostgreSQL database dump complete
--
