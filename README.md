# Roper RDB Project #

## Description ##
***
This project holds the database setup for the new Roper Center website. The database configuration uses Liquibase

## Liquibase Setup ##
***
In a terminal window, go to the directory and execute the command:
~~~~
mvn liquibase:update
~~~~

This will run all the Liquibase scripts identified in the master file.